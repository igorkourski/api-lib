package api.util;

public class ServiceFactory {
	
	private static Service service;
	
	public static Service get(){
		if (service==null){
			service = new Service();
		}
		return service;
	}
	
}
