package api;

import api.util.ServiceFactory;

public class Test {
	
	public static void main(String[] args) {
		
		long start = System.nanoTime();
		int threads = 100;
		int positive = 0;
		
		for (int i=0;i<threads;i++){	
			

			if (ServiceFactory.get().response(i))
				positive++;
		}
		
		long whole = (System.nanoTime() - start)/1000;
		long medium = whole/threads;
		
		System.out.println("\nTime of work:" + whole + "\nTime of thread:" + medium);
		System.out.println("Success: " + positive + " out of " + threads);
		System.out.println("Average:" + ServiceFactory.get().getAverage() + "ms");	

	}

}
